#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <stdint.h>
#include <lwip/ip.h>

#include "player_cmd.h"
#include "battery.h"

// BSSID used to broadcast an event to all players
#define PLAYER_BSSID_ALL (const uint8_t[6]){0xFF,0xFF,0xFF,0xFF,0xFF,0xFF}

// When the first player in READY state pushes the button, he will be sent back
// a trig command, and remaining players will receive a block command.
// Event descriptions:
// ADD: Sent by host when a player enters
// DEL: Sent by host when a player is removed
// IP_UPDATE: Sent by host when a player gets an updated IP address
// GAME_START: Sent by host when a game is started
// COLOR: Sent by host to configure player color
// START_ACK: Sent by the player to acknowledge a START command
// TRIGGER: Sent by the player when the button is pressed in READY state
// BLOCK: Sent by host when game has ended and player has lost
// BLOCK_ACK: Sent by the player to acknowledge a BLOCK command
#define PLAYER_EVENT_TABLE(X_MACRO) \
	X_MACRO(START_ACK, start_ack) \
	X_MACRO(TRIGGER, trigger) \
	X_MACRO(BLOCK_ACK, block_ack) \
	X_MACRO(BATT_LOW_CHARGE, batt_low_charge) \
	X_MACRO(BATT_LOW_DISCHARGE, batt_low_discharge) \
	X_MACRO(BATT_MID_CHARGE, batt_mid_charge) \
	X_MACRO(BATT_MID_DISCHARGE, batt_mid_discharge) \
	X_MACRO(BATT_HIGH_CHARGE, batt_high_charge) \
	X_MACRO(BATT_HIGH_DISCHARGE, batt_high_discharge) \
	X_MACRO(ADD, add) \
	X_MACRO(DEL, del) \
	X_MACRO(GAME_START, game_start) \
	X_MACRO(COLOR, color) \
	X_MACRO(INTENSITY, intensity) \
	X_MACRO(BLOCK, block) \

#define X_AS_PLAYER_EVENT_ENUM(u_name, l_name) PLAYER_EVENT_ ## u_name,
enum player_event_type {
	PLAYER_EVENT_TABLE(X_AS_PLAYER_EVENT_ENUM)
	__PLAYER_EVENT_MAX
};

struct player_event {
	enum player_event_type event;
	uint8_t bssid[6]; // Player BSSID or PLAYER_BSSID_ALL
	union {
		ip4_addr_t ip;
		enum player_color color;
		enum player_intensity intensity;
	};
};

#define PLAYER_EVENT_MAX_LEN (sizeof(struct player_event) + sizeof(ip4_addr_t))

struct player {
	int number;
	uint8_t bssid[6];
	enum player_color color;
	ip4_addr_t ip;
};

// Events for the global FSM
enum player_game_event_type {
	PLAYER_GAME_EVENT_WIN,
	PLAYER_GAME_EVENT_BATT,
	__PLAYER_GAME_EVENT_MAX
};

struct player_batt_event {
	enum bat_event event;
	enum bat_status status;
};

struct player_game_event {
	enum player_game_event_type event;
	uint8_t bssid[6];
	union {
		uint32_t nothing[0];
		struct player_batt_event batt;
	};
};

typedef void (*player_game_event_cb)(const struct player_game_event *player);

int player_init(player_game_event_cb event_cb);
void player_deinit(void);
uint_fast8_t player_num_get(void);
void player_parse(struct player_event *event);

// Returned data is allocated and must be freed when no longer needed
uint_fast8_t player_get_bssids(uint8_t **bssids);

#endif /*_PLAYER_H_*/

