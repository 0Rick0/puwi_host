#ifndef __PLAYER_CMD_H__
#define __PLAYER_CMD_H__

#include <stdint.h>

enum player_cmd {
	PLAYER_CMD_START,
	PLAYER_CMD_BLOCK,
	PLAYER_CMD_TRIGGER_ACK,
	PLAYER_CMD_COLOR
};

enum player_color {
	PLAYER_COLOR_RED,
	PLAYER_COLOR_GREEN,
	PLAYER_COLOR_BLUE,
	PLAYER_COLOR_YELLOW,
	__PLAYER_COLOR_MAX
};

enum player_intensity {
	PLAYER_INTENSITY_0,
	PLAYER_INTENSITY_20,
	PLAYER_INTENSITY_40,
	PLAYER_INTENSITY_60,
	PLAYER_INTENSITY_80,
	PLAYER_INTENSITY_100,
	__PLAYER_INTENSITY_MAX
};

struct player_led {
	enum player_color color;
	enum player_intensity intensity;
};

struct player_cmd_req {
	enum player_cmd cmd;
	uint32_t player_num;
	struct player_led led;
};


#endif /*__PLAYER_CMD_H__*/

