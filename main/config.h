#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <stdint.h>

enum cfg_color {
	CFG_COLOR_RED = 0,
	CFG_COLOR_GREEN,
	CFG_COLOR_BLUE,
	CFG_COLOR_YELLOW,
	__CFG_COLOR_MAX
};

enum cfg_intensity {
	CFG_INTENSITY_0 = 0,
	CFG_INTENSITY_20,
	CFG_INTENSITY_40,
	CFG_INTENSITY_60,
	CFG_INTENSITY_80,
	CFG_INTENSITY_100,
	__CFG_INTENSITY_MAX
};

enum cfg_tg_event_mask {
	CFG_TG_EVENT_BATT = 1,
	CFG_TG_EVENT_ROUND = 2,
	CFG_TG_EVENT_ALL = 3
};

struct cfg_entry {
	uint8_t bssid[6];
	char name[34];
	enum cfg_color color;
};

struct cfg {
	uint8_t md5[16];
	enum cfg_intensity bar_intensity;
	enum cfg_tg_event_mask tg_event;
	uint32_t num_entries;
	struct cfg_entry entries[];
};

int cfg_init(void);
void cfg_deinit(void);
const struct cfg *cfg_get(void);
int cfg_save(struct cfg *config);
const struct cfg_entry *cfg_search(const struct cfg *config,
		const uint8_t bssid[6]);
uint32_t cfg_length(const struct cfg *config);

#endif /*__CONFIG_H__*/
