#ifndef __TELEGRAM_H__
#define __TELEGRAM_H__

#include <bate.h>
#include "config.h"

enum tg_event_type {
	TG_EVENT_PARSER_READY = 0,
	TG_EVENT_PARSER_END,
	TG_EVENT_COLOR_CFG,
	TG_EVENT_INTENSITY_CFG,
	TG_EVENT_TG_ENA_START,
	TG_EVENT_TG_DIS_START,
	__TG_EVENT_MAX
};

enum tg_color {
	TG_COLOR_RED = CFG_COLOR_RED,
	TG_COLOR_GREEN = CFG_COLOR_GREEN,
	TG_COLOR_BLUE = CFG_COLOR_BLUE,
	TG_COLOR_YELLOW = CFG_COLOR_YELLOW,
	TG_COLOR_PURPLE,
	TG_COLOR_WHITE,
	__TG_COLOR_MAX
};

static const char * const color_str[__TG_COLOR_MAX] = {
	"🔴", "🟢", "🔵", "🟡", "🟣", "🔘"
};

struct tg_color_cfg_data {
	uint8_t bssid[6];
	enum cfg_color color;
};

union tg_data {
	struct tg_color_cfg_data color_cfg;
	enum cfg_intensity intensity;
};

typedef void (*tg_event_cb)(enum tg_event_type event, union tg_data *data);

int tg_init(void);
void tg_deinit(void);
int tg_parser_start(tg_event_cb event_cb);
void tg_parser_stop(void);
int tg_notify(const char *msg, enum bate_text_mode parse_mode);

#endif /*__TELEGRAM_H__*/
