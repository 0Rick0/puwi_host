#include <string.h>

#include <nvs_flash.h>
#include <esp_wifi.h>
#include <esp_timer.h>
#include <FreeRTOS.h>
#include <freertos/task.h>
#include <driver/gpio.h>

#include <yamcha.h>

#include "util.h"
#include "wifi_apsta.h"
#include "net_ev.h"
#include "player.h"
#include "control.h"
#include "config.h"
#include "telegram.h"
#include "app_prov.h"
#include "battery.h"

#define PUWI_HOST_VERSION "1.0"

#define PROG_PIN 0
#define PROG_PIN_MASK (1UL<<PROG_PIN)

#define INPUT_PIN 13
#define INPUT_PIN_MASK (1UL<<INPUT_PIN)

#define NEOPIXEL_PIN 12

// 100 ms debounce time
#define DEBOUNCE_US	100000

static IRAM_ATTR void button_event_isr(void *arg)
{
	static uint64_t last = 0;
	uint64_t current = esp_timer_get_time();

	if ((current - last) > DEBOUNCE_US) {
		struct ctrl_event ctrl = {
			.event = CTRL_EVENT_BUTTON,
			.data = arg
		};
		ctrl_event_from_isr(&ctrl);
		last = current;
	}
}

static void player_event_cb(const struct player_game_event *player)
{
	struct ctrl_event ctrl = {};

	switch (player->event) {
	case PLAYER_GAME_EVENT_WIN:
		ctrl.event = CTRL_EVENT_END;
		ctrl.data = memdup(player->bssid, 6);
		ctrl_event(&ctrl);
		break;

	case PLAYER_GAME_EVENT_BATT:
		ctrl.event = CTRL_EVENT_BATTERY_PLAYER;
		ctrl.data = malloc(sizeof(struct ctrl_data_battery_player));
		memcpy(ctrl.data->battery_player.bssid, player->bssid, 6);
		ctrl.data->battery_player.event = player->batt.event;
		ctrl.data->battery_player.status = player->batt.status;
		ctrl_event(&ctrl);
		break;

	default:
		break;
	}
}

static void battery_event_cb(enum bat_event event, enum bat_status status,
		uint32_t voltage_millis)
{
	struct ctrl_event ctrl = {
		.event = CTRL_EVENT_BATTERY_HOST
	};

	ctrl.data = calloc(1, sizeof(struct ctrl_data_battery_host));
	ctrl.data->battery_host.event = event;
	ctrl.data->battery_host.status = status;
	ctrl.data->battery_host.voltage_millis = voltage_millis;

	ctrl_event(&ctrl);
}

static void telegram_cb(enum tg_event_type event, union tg_data *data)
{
	struct ctrl_event ctrl = {};

	switch (event) {
	case TG_EVENT_PARSER_READY:
		ctrl.event = CTRL_EVENT_SERVER_READY;
		break;

	case TG_EVENT_PARSER_END:
		ctrl.event = CTRL_EVENT_SERVER_DROP;
		break;

	case TG_EVENT_COLOR_CFG:
		ctrl.event = CTRL_EVENT_COLOR_CONFIG;
		ctrl.data = calloc(1, sizeof(struct ctrl_data_color_config));
		memcpy(ctrl.data->color_config.bssid, data->color_cfg.bssid, 6);
		ctrl.data->color_config.color = data->color_cfg.color;
		break;

	case TG_EVENT_INTENSITY_CFG:
		ctrl.event = CTRL_EVENT_INTENSITY_CONFIG;
		ctrl.data = calloc(1, sizeof(struct ctrl_data_intensity_config));
		ctrl.data->intensity_config.intensity = data->intensity;
		break;

	case TG_EVENT_TG_ENA_START:
		ctrl.event = CTRL_EVENT_START_WITH_TG;
		break;

	case TG_EVENT_TG_DIS_START:
		ctrl.event = CTRL_EVENT_START_WITHOUT_TG;
		break;

	default:
		LOGW("unknown event %d", event);
		return;
	}

	ctrl_event(&ctrl);
}

static void apsta_event_cb(enum wifi_event event, union wifi_event_data *data)
{
	struct ctrl_event ctrl = {};
	size_t data_len = 0;

	switch (event) {
	case WIFI_EVENT_STA_ADD:
		ctrl.event = CTRL_EVENT_STA_ASSOC;
		data_len = sizeof(struct ctrl_data_sta_assoc);
		break;

	case WIFI_EVENT_STA_DEL:
		ctrl.event = CTRL_EVENT_STA_DISASSOC;
		data_len = sizeof(struct ctrl_data_sta_disassoc);
		break;

	case WIFI_EVENT_STA_IP:
		ctrl.event = CTRL_EVENT_STA_IP;
		data_len = sizeof(struct ctrl_data_sta_ip);
		break;

	case WIFI_EVENT_AP_GOT_IP:
		// We are connected to AP, start Telegram bot and return
		// without sending any data
		tg_parser_start(telegram_cb);
		return;

	default:
		LOGW("unused apsta event %d", event);
		return;
	}

	ctrl.data = memdup(data, data_len);

	ctrl_event(&ctrl);
}

static void button_cfg(bool isr_enable)
{
	static gpio_config_t cfg = {
		.mode = GPIO_MODE_INPUT,
		.intr_type = GPIO_INTR_NEGEDGE,
		.pull_up_en = GPIO_PULLUP_ENABLE,
		.pin_bit_mask = INPUT_PIN_MASK | PROG_PIN_MASK
	};

	if (isr_enable) {
		cfg.intr_type = GPIO_INTR_NEGEDGE;
	}
	gpio_config(&cfg);

	if (isr_enable) {
		//install gpio isr service
		gpio_install_isr_service(0);
		//hook isr handler for specific gpio pin
		gpio_isr_handler_add(INPUT_PIN, button_event_isr, (void*)INPUT_PIN);
		gpio_isr_handler_add(PROG_PIN, button_event_isr, (void*)PROG_PIN);
	}
}

static void app_start(void)
{
	const struct bat_cfg cfg = {
		.cb = battery_event_cb,
		.threshold = BAT_THRESHOLD_DEFAULT,
		.hysteresis = 50
	};

	bat_start(&cfg);
	wifi_init_softap(apsta_event_cb);
	ESP_ERROR_CHECK(esp_wifi_set_ps(WIFI_PS_NONE));
	ESP_ERROR_CHECK(player_init(player_event_cb));
	ESP_ERROR_CHECK(ctrl_start());
	tg_init();
}

static esp_err_t prov_event_handler(void *ctx, system_event_t *event)
{
	app_prov_event_handler(ctx, event);

	switch(event->event_id) {
	case SYSTEM_EVENT_STA_START:
		esp_wifi_connect();
		break;

	default:
		break;

	}
	return ESP_OK;
}

static const struct yam_cmd_req prov_led_cfg = {
	.mode = YAM_CMD_BREATH,
	.breath = {
		.pixels = {
			{.r = 84}, {.r = 84}, {.r = 84}, {.r = 84},
			{.b = 84}, {.b = 84}, {.b = 84}, {.b = 84}
		},
		.delay_ms = 32
	}
};

static void provision_start(void)
{
	yam_command(&prov_led_cfg);
	tcpip_adapter_init();
	ESP_ERROR_CHECK(esp_event_loop_init(prov_event_handler, NULL));

	LOGI("starting WiFi SoftAP provisioning");
	app_prov_start_softap_provisioning("PuWi Config",
			"Pr0v1sIOn", 1, NULL);
}

void app_main(void)
{
	esp_err_t ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);
	ESP_ERROR_CHECK(cfg_init());

	LOGI("STARTING PUWI HOST VERSION " PUWI_HOST_VERSION);

	button_cfg(false);
	yam_init(NEOPIXEL_PIN);

	if (0 == gpio_get_level(INPUT_PIN)) {
		provision_start();
	} else {
		button_cfg(true);
		app_start();
	}
}

