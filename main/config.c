#include <string.h>
#include <esp_partition.h>
#include <mbedtls/md5.h>

#include "util.h"
#include "config.h"

#define CFG_PART_TYPE 0x40
#define CFG_PART_SUBTYPE 0
#define CFG_PART_NAME "cfg"

/// Base physical address for the SPI flash chip
#define SPI_FLASH_BASE	0x40200000
/// Compute the physical address for the specified SPI flash cip address
#define SPI_FLASH_ADDR(flash_addr)	(SPI_FLASH_BASE + (flash_addr))
/// \warning alignment to type is not enforced
#define SPI_FLASH_PTR(type, part, off)	((type*)SPI_FLASH_ADDR((part->address + (off))))

static const esp_partition_t *p = NULL;

static esp_err_t config_default_set(void)
{
	struct cfg new_cfg = {
		.bar_intensity = CFG_INTENSITY_60,
		.tg_event = CFG_TG_EVENT_ALL,
		.num_entries = 0
	};
	return cfg_save(&new_cfg);
}

int cfg_init(void)
{
	uint8_t md5[16];

	p = esp_partition_find_first(CFG_PART_TYPE, CFG_PART_SUBTYPE,
			CFG_PART_NAME);
	if (!p) {
		return ESP_FAIL;
	}

	// If no previous config was stored, initialize structure
	const struct cfg *cfg = cfg_get();
	if (0xFFFFFFFF == cfg->num_entries) {
		LOGW("no previous config found, initialising...");
		return config_default_set();
	}

	mbedtls_md5(((unsigned char*)cfg) + 16, cfg_length(cfg) - 16, md5);
	if (0 != memcmp(cfg->md5, md5, 16)) {
		LOGW("configuration MD5 does not match, initialising...");
		return config_default_set();
	}
	
	return ESP_OK;
}

void cfg_deinit(void)
{
	// It seems partition data needs not to be freed
	p = NULL;
}

const struct cfg *cfg_get(void)
{
	return SPI_FLASH_PTR(const struct cfg, p, 0);
}

int cfg_save(struct cfg *config)
{
	const size_t len = cfg_length(config);
	if (len > p->size) {
		LOGE("config length %u does not fit in partition", len);
		return 1;
	}
	if (ESP_OK != esp_partition_erase_range(p, 0, p->size)) {
		LOGE("partition erase failed");
		return 1;
	}
	mbedtls_md5(((unsigned char*)config) + 16, len - 16, config->md5);
	if (ESP_OK != esp_partition_write(p, 0, config, len)) {
		LOGE("partition write failed");
		return 1;
	}

	LOGI("configuration saved");
	return 0;
}

const struct cfg_entry *cfg_search(const struct cfg *config,
		const uint8_t bssid[6])
{
	int i;
	const struct cfg_entry *entries = config->entries;

	for (i = 0; i < config->num_entries; i++) {
		if (0 == memcmp(bssid, entries[i].bssid, 6)) {
			return &entries[i];
		}
	}

	return NULL;
}

uint32_t cfg_length(const struct cfg *config)
{
	return sizeof(struct cfg) + config->num_entries *
		sizeof(struct cfg_entry);
}
