#include <string.h>

#include <FreeRTOS.h>
#include <freertos/task.h>
#include <esp_wifi.h>

#include <bate.h>

#include "player.h"
#include "linux_list.h"
#include "util.h"

#include "telegram.h"

// Task must be lower priority than network and event parsing tasks, to avoid
// introducing delays in player input parsing
#define TG_TASK_PRIO (tskIDLE_PRIORITY + 1)
#define TG_TASK_STACK 4096
#define TG_CHAT_ID_MAX_LEN 31

#define TG_ERROR_MAX 5

static struct {
	char chat_id[TG_CHAT_ID_MAX_LEN + 1];
	tg_event_cb event_cb;
	bool parser_enable;
	bool wifi_disconnect;
} tg = {};

#define COMMAND_TABLE(X_MACRO) \
	X_MACRO(START, start, /start) \
	X_MACRO(PLAYERS, players, Players) \
	X_MACRO(CONFIGURE, configure, Configure) \
	X_MACRO(BEGIN_WITH_TELEGRAM, begin_with_telegram, Begin with Telegram) \
	X_MACRO(BEGIN_WITHOUT_TELEGRAM, begin_without_telegram, Begin without Telegram) \

#define X_AS_CMD_ENUM(uname, lname, str) CMD_ ## uname,
enum cmd {
	COMMAND_TABLE(X_AS_CMD_ENUM)
	__CMD_MAX
};

#define X_AS_CMD_STRINGS(uname, lname, str) #str,
static const char * const keyb[__CMD_MAX + 1] = {
	COMMAND_TABLE(X_AS_CMD_STRINGS)
	NULL
};
static const unsigned int cols[] = {3, 1, 1};

#define X_AS_CMD_PARSE_FN_PROTO(uname, lname, str) \
	static void cmd_ ## lname ## _parse(const char*, \
		const struct bate_message*, \
		const struct bate_chat*);
COMMAND_TABLE(X_AS_CMD_PARSE_FN_PROTO);

#define X_AS_CMD_PARSE_FN(uname, lname, str) cmd_ ## lname ## _parse,
static void (*message_parse_fn[__CMD_MAX])(const char *chat_id,
		const struct bate_message *msg,
		const struct bate_chat *chat) = {
	COMMAND_TABLE(X_AS_CMD_PARSE_FN)
};

enum cb_query_type {
	QUERY_PLAYER_COLOR,
	QUERY_BAR_INTENSITY,
	QUERY_TG_NOTIF,
	QUERY_PLAYER,
	QUERY_COLOR,
	QUERY_INTENSITY,
	QUERY_NOTIF,
	__QUERY_MAX
};

static const char * const cb_query_str[__QUERY_MAX] = {
	"player_color",
	"bar_intensity",
	"tg_notif",
	"player_",
	"color_",
	"intensity_",
	"notif_"
};

static const char * const intensity_str[__CFG_INTENSITY_MAX] = {
	"OFF", "20%", "40%", "60%", "80%", "100%"
};

static struct bate_bot_info *bot_get(void)
{
	cJSON *json = NULL;
	struct bate_bot_info *bot = NULL;

	bate_me_get(&json);
	bot = bate_me_parse(json);
	cJSON_Delete(json);

	return bot;
}

static void hello_msg(const char *chat_id)
{
	const char * const text = "*Welcome to PuWi!*\n"
		"Do you want to play? 🔘😎";

	bate_keyboard_reply(chat_id, text, BATE_TEXT_MARKDOWN,
			(const char**)keyb, 3, cols, BATE_KEYB_RESIZE, NULL);
}

static void cmd_start_parse(const char *chat_id, const struct bate_message *msg,
		const struct bate_chat *chat)
{
	UNUSED_PARAM(chat);
	UNUSED_PARAM(msg);
	hello_msg(chat_id);
}

static void unsupported_command(const char *chat_id,
		const struct bate_chat *chat)
{
	char msg[128];

	snprintf(msg, 128, "Sorry _%s_, I do not understand that!",
			chat->username);
	bate_msg_send(chat_id, msg, BATE_TEXT_MARKDOWN, NULL);
}

static void cmd_players_parse(const char *chat_id, const struct bate_message *msg,
		const struct bate_chat *chat)
{
	UNUSED_PARAM(chat);
	UNUSED_PARAM(msg);
	uint8_t *bssids = NULL;
	uint_fast8_t num_players = player_get_bssids(&bssids);
	char *text = NULL;
	char *pos_str;
	uint8_t *pos_bssid;
	// mac + ": " + color (utf-8)
	const size_t entry_len = 4 + 2 + 18 + 1;
	const struct cfg *cfg = cfg_get();
	const struct cfg_entry *player;
	enum cfg_color color;

	if (!num_players) {
		bate_msg_send(chat_id, "There are no players currently 🤷",
				BATE_TEXT_PLAIN, NULL);
		goto out;
	}

	// Message length: 10 ("Players:\n" + '\0) plus 18 * number of players
	// (17 bytes MAC address plus \n)
	text = malloc(10 + num_players * entry_len);
	pos_str = text;
	pos_bssid = bssids;
	memcpy(pos_str, "Players:\n", 9);
	pos_str += 9;

	for (int i = 0; i < num_players; i++, pos_str += entry_len,
			pos_bssid += 6) {
		player = cfg_search(cfg, pos_bssid);
		color = player ? player->color : __CFG_COLOR_MAX;
		sprintf(pos_str, "%s `" MACSTR "`\n", color_str[color],
				MAC2STR(pos_bssid));
	}

	bate_msg_send(chat_id, text, BATE_TEXT_MARKDOWN, NULL);

out:
	free(bssids);
	free(text);
}

static void cmd_configure_parse(const char *chat_id,
		const struct bate_message *msg,
		const struct bate_chat *chat)
{
	UNUSED_PARAM(msg);
	UNUSED_PARAM(chat);

	const struct bate_inline_keyboard_button keyb[] = {
		{
			.type = BATE_INLINE_KEYB_TYPE_CALLBACK_DATA,
			.text = "Set player color",
			.callback_data = cb_query_str[QUERY_PLAYER_COLOR]
		}, {
			.type = BATE_INLINE_KEYB_TYPE_CALLBACK_DATA,
			.text = "Set LED bar intensity",
			.callback_data = cb_query_str[QUERY_BAR_INTENSITY]
		}, {
			.type = BATE_INLINE_KEYB_TYPE_CALLBACK_DATA,
			.text = "Telegram notifications",
			.callback_data = cb_query_str[QUERY_TG_NOTIF]
		}
	};
	const unsigned int cols[] = {1, 1, 1};

	bate_inline_keyboard_reply(chat_id, "Sure, what do you want to "
			"configure?", BATE_TEXT_PLAIN, keyb, 3, cols, NULL);
}

static void no_players_msg(const char *chat_id)
{
	bate_msg_send(chat_id, "Sorry, there are no players connected. "
			"Connect a PuWi player and try again.",
			BATE_TEXT_PLAIN, NULL);
}

static void cmd_begin_with_telegram_parse(const char *chat_id,
		const struct bate_message *msg,
		const struct bate_chat *chat)
{
	UNUSED_PARAM(msg);
	UNUSED_PARAM(chat);

	if (player_num_get() <= 0) {
		no_players_msg(chat_id);
		return;
	}
	tg.parser_enable = false;
	strncpy(tg.chat_id, chat_id, TG_CHAT_ID_MAX_LEN);
	tg.chat_id[TG_CHAT_ID_MAX_LEN] = '\0';
	if (tg.event_cb) {
		tg.event_cb(TG_EVENT_TG_ENA_START, NULL);
	}
	bate_msg_send(chat_id, "Great, lets play!", BATE_TEXT_PLAIN, NULL);
}

static void cmd_begin_without_telegram_parse(const char *chat_id,
		const struct bate_message *msg,
		const struct bate_chat *chat)
{
	UNUSED_PARAM(msg);
	UNUSED_PARAM(chat);

	if (player_num_get() <= 0) {
		no_players_msg(chat_id);
		return;
	}
	tg.parser_enable = false;
	tg.wifi_disconnect = true;
	tg.chat_id[0] = '\0';
	if (tg.event_cb) {
		tg.event_cb(TG_EVENT_TG_DIS_START, NULL);
	}
	bate_msg_send(chat_id, "OK, over and out!", BATE_TEXT_PLAIN, NULL);
}

static void message_parse(const char *chat_id, const struct bate_message *msg,
		const struct bate_chat *chat)
{
	int command;

	if (msg && msg->text) {
		command = token_index((const char**)keyb, msg->text);
		if (command >= 0) {
			message_parse_fn[command](chat_id, msg, chat);
		} else {
			unsupported_command(chat_id, chat);
		}
	} else {
		LOGW("Ignoring message with no text");
	}
}

static void message_json_parse(const cJSON *msg_json)
{

	char chat_id[32];
	struct bate_message *msg = bate_message_parse(msg_json);
	struct bate_chat *chat = bate_chat_parse(msg->chat);

	snprintf(chat_id, 32, "%ld", chat->id);

	// Message from the client allowed to use the bot
	message_parse(chat_id, msg, chat);

	bate_message_free(msg);
	bate_chat_free(chat);
}

static void query_intensity_choose(const char *chat_id, const char* data)
{
	char msg[64];
	enum cfg_intensity intensity;

	// We guess the value by just checking the first character.
	// Default value is 60%
	switch (data[0]) {
		case 'O':
			// flowdown
		case '0':
			intensity = CFG_INTENSITY_0;
			break;

		case '2':
			intensity = CFG_INTENSITY_20;
			break;

		case '4':
			intensity = CFG_INTENSITY_40;
			break;

		case '8':
			intensity = CFG_INTENSITY_80;
			break;

		case '1':
			intensity = CFG_INTENSITY_100;
			break;

		default:
			intensity = CFG_INTENSITY_60;
	}

	if (tg.event_cb) {
		union tg_data event_data = {};
		event_data.intensity = intensity;
		tg.event_cb(TG_EVENT_INTENSITY_CFG, &event_data);
	}

	snprintf(msg, 40, "Intensity set to *%s*. Check it out!", data);
	bate_msg_send(chat_id, msg, BATE_TEXT_MARKDOWN, NULL);
}

static void query_intensity_set(const char *chat_id)
{
	char msg[80];
	struct bate_inline_keyboard_button keyb[__CFG_INTENSITY_MAX] = {};
	const unsigned int cols[] = {6};
	char pool[__CFG_INTENSITY_MAX * (10 + 4 + 1)];
	char *pos = pool;
	const struct cfg *cfg = cfg_get();

	snprintf(msg, 80, "Current brightness is *%s*. How much brightness do "
			"you want?", intensity_str[cfg->bar_intensity]);

	for (int i = 0; i < __CFG_INTENSITY_MAX; i++, pos += 10 + 4 + 1) {
		sprintf(pos, "intensity_%s", intensity_str[i]);
		keyb[i].type = BATE_INLINE_KEYB_TYPE_CALLBACK_DATA;
		keyb[i].text = intensity_str[i];
		keyb[i].callback_data = pos;
	}

	bate_inline_keyboard_reply(chat_id, msg, BATE_TEXT_MARKDOWN, keyb, 1,
			cols, NULL);
}

static void query_player_color_choose(const char *chat_id)
{
	uint8_t *bssids = NULL;
	const uint_fast8_t num_players = player_get_bssids(&bssids);

	if (!bssids || !num_players) {
		no_players_msg(chat_id);
		goto out;
	}

	struct bate_inline_keyboard_button *keyb = calloc(num_players,
			sizeof(struct bate_inline_keyboard_button));
	const size_t entry_len = 7 + 18 + 2 + 4;
	char *macs = malloc(num_players * entry_len);
	unsigned int *cols = malloc(num_players * sizeof(unsigned int));
	const struct cfg *cfg = cfg_get();

	char *mac = macs;
	uint8_t *bssid = bssids;
	for (int i = 0; i < num_players; i++, mac += entry_len, bssid += 6) {
		const struct cfg_entry *player = cfg_search(cfg, bssid);
		const enum cfg_color color_idx = player ? player->color :
			__CFG_COLOR_MAX;
		sprintf(mac, "player_" MACSTR ": %s", MAC2STR(bssid),
				color_str[color_idx]);
		keyb[i].type = BATE_INLINE_KEYB_TYPE_CALLBACK_DATA;
		keyb[i].text = mac + 7;
		keyb[i].callback_data = mac;
		cols[i] = 1;
	}

	bate_inline_keyboard_reply(chat_id, "Which PuWi do you want to config?",
			BATE_TEXT_PLAIN, keyb, num_players, cols, NULL);
	free(cols);
	free(macs);
	free(keyb);
out:
	free(bssids);
}

static void query_player_choose(const char *chat_id, const char* mac)
{
	// Space for 4 colors, each: "color_" + mac + color
	const size_t data_len = 6 + 18 + 4;
	char data[__CFG_COLOR_MAX * data_len];
	char *pos = data;
	struct bate_inline_keyboard_button keyb[__CFG_COLOR_MAX];
	unsigned int cols = __CFG_COLOR_MAX;
	char msg[36];

	for (int i = 0; i < __CFG_COLOR_MAX; i++, pos += data_len) {
		sprintf(pos, "color_%.*s%s", 17, mac, color_str[i]);
		keyb[i].type = BATE_INLINE_KEYB_TYPE_CALLBACK_DATA;
		keyb[i].text = color_str[i];
		keyb[i].callback_data = pos;
	}

	snprintf(msg, 36, "Select color for %s", mac);
	bate_inline_keyboard_reply(chat_id, msg, BATE_TEXT_PLAIN, keyb,
			1, &cols, NULL);
}

static void query_color_choose(const char *chat_id, const char* data)
{
	uint8_t bssid[6];
	char msg[32];

	// data contains the MAC address and the chosen color
	if (mac_to_bin(data, bssid)) {
		LOGW("invalid BSSID %s", data);
		return;
	}

	// Skip mac to go to color
	data += 17;
	enum cfg_color color = token_index((const char**)color_str, data);
	if (color < 0) {
		LOGW("invalid color %s", data);
		return;
	}
	if (tg.event_cb) {
		union tg_data event_data = {};
		memcpy(event_data.color_cfg.bssid, bssid, 6);
		event_data.color_cfg.color = color;
		tg.event_cb(TG_EVENT_COLOR_CFG, &event_data);
	}
	snprintf(msg, 32, "OK, setting color %s!", data);
	bate_msg_send(chat_id, msg, BATE_TEXT_PLAIN, NULL);
}

static void query_tg_notif_choose(const char *chat_id)
{
	char msg[192];
	const char *str = "only battery";
	const struct bate_inline_keyboard_button keyb[] = {
		{
			.type = BATE_INLINE_KEYB_TYPE_CALLBACK_DATA,
			.text = "Only battery notifications",
			.callback_data = "notif_batt"
		}, {
			.type = BATE_INLINE_KEYB_TYPE_CALLBACK_DATA,
			.text = "Battery and round notifications",
			.callback_data = "notif_all"
		}
	};
	const unsigned int cols[] = {1, 1};

	if (cfg_get()->tg_event & CFG_TG_EVENT_ROUND) {
		str = "all";
	}
	snprintf(msg, 192, "When playing with Telegram notifications enabled, "
			"do you want only battery notifications, or all of "
			"them?\n(Currently configured for %s)", str);

	bate_inline_keyboard_reply(chat_id, msg, BATE_TEXT_PLAIN,
			keyb, 2, cols, NULL);
}

static void query_notif_choose(const char *chat_id, const char *data)
{
	const struct cfg *cfg = cfg_get();
	enum cfg_tg_event_mask notif_mask = CFG_TG_EVENT_ALL;
	const char *str = "all";
	char msg[64];

	if (0 == strcmp("batt", data)) {
		notif_mask = CFG_TG_EVENT_BATT;
		str = "only battery";
	}

	if (notif_mask != cfg->tg_event) {
		const size_t len = cfg_length(cfg);
		struct cfg *new_cfg = memdup(cfg, len);

		new_cfg->tg_event = notif_mask;
		cfg_save(new_cfg);
		free(new_cfg);
	}

	snprintf(msg, 64, "Done. You will receive %s notifications.", str);
	bate_msg_send(chat_id, msg, BATE_TEXT_PLAIN, NULL);
}

static void callback_query_json_parse(const cJSON *cbq_json)
{
	char chat_id[32];
	struct bate_callback_query *query =
		bate_callback_query_parse(cbq_json);

	if (!query) {
		LOGW("something went wrong processing query");
		return;
	}

	struct bate_from *from = bate_from_parse(query->from);
	if (!from) {
		LOGW("something went wrong processing from");
		goto err_from;
	}
	snprintf(chat_id, 32, "%ld", from->id);
	bate_from_free(from);

	bate_callback_query_answer(query->id, NULL, false, NULL, 0, NULL);

	if (0 == strcmp(query->data, cb_query_str[QUERY_PLAYER_COLOR])) {
		query_player_color_choose(chat_id);
	} else if (0 == strcmp(query->data,
				cb_query_str[QUERY_BAR_INTENSITY])) {
		query_intensity_set(chat_id);
	} else if (0 == strcmp(query->data, cb_query_str[QUERY_TG_NOTIF])) {
		query_tg_notif_choose(chat_id);
	} else if (query->data == strstr(query->data,
				cb_query_str[QUERY_INTENSITY])) {
		// Advance 10 to skip "intensity_"
		query_intensity_choose(chat_id, query->data + 10);
	} else if (query->data == strstr(query->data,
				cb_query_str[QUERY_PLAYER])) {
		// Advance 7 to skip "player_"
		query_player_choose(chat_id, query->data + 7);
	} else if (query->data == strstr(query->data,
				cb_query_str[QUERY_COLOR])) {
		// Advance 6 to skip "color_"
		query_color_choose(chat_id, query->data + 6);
	} else if (query->data == strstr(query->data,
				cb_query_str[QUERY_NOTIF])) {
		// Advance 6 to skip "notif_"
		query_notif_choose(chat_id, query->data + 6);
	} else {
		LOGW("unknown callback query %s", query->data);
	}

err_from:
	bate_callback_query_free(query);
}

static long process_update(cJSON *update)
{
	long next;
	struct bate_update *up = bate_update_parse(update);
	LOGI("update_id: %ld", up->update_id);

	switch (up->type) {
	case BATE_UPDATE_TYPE_MESSAGE:
		message_json_parse(up->message);
		break;

	case  BATE_UPDATE_TYPE_CALLBACK_QUERY:
		callback_query_json_parse(up->callback_query);
		break;

	default:
		LOGD("got unsupported update %d", up->type);
		break;
	}


	next = up->update_id + 1;
	bate_update_free(up);

	return next;
}

void telegram_tsk(void *arg)
{
	UNUSED_PARAM(arg);

	struct bate_bot_info *bot = NULL;
	long next_update = 0;
	cJSON *root, *updates, *update;
	uint32_t errors = 0;

	LOGI("Telegram bot start");
	while (!(bot = bot_get())) {
		vTaskDelay(pdMS_TO_TICKS(30000));
	}

	LOGI("connected: id = %ld, first_name = %s, username = %s", bot->id,
			bot->first_name, bot->username);

	while (tg.parser_enable && errors < TG_ERROR_MAX) {
		if (200 == bate_updates_get(next_update,
					300000, &root)) {
			updates = json_get_array(root, "result");
			cJSON_ArrayForEach(update, updates) {
				next_update = process_update(update);
			}
			cJSON_Delete(root);
			errors = 0;
		} else {
			errors++;
		}
	}
	if (!tg.parser_enable) {
		// Make a final request to mark the last message as read
		bate_updates_get(next_update, 0, NULL);
		LOGI("Telegram bot end due to user request");
	} else {
		// Exited due to errors, reboot
		LOGE("Telegram bot exit because of error, rebooting");
		esp_wifi_stop();
		esp_restart();
	}

	LOGW("Bot task has ended");
	bate_me_free(bot);
	if (tg.wifi_disconnect) {
		bate_cleanup();
		vTaskDelayMs(10000);
		esp_wifi_disconnect();
	}
	vTaskDelete(NULL);
}

int tg_init(void)
{
	// Nothing to do
	return 0;
}

void tg_deinit(void)
{
	// TODO: stop task if running
}

int tg_parser_start(tg_event_cb event_cb)
{
	BaseType_t result;

	tg.event_cb = event_cb;
	tg.parser_enable = true;

	result = xTaskCreate(telegram_tsk, "telegram", TG_TASK_STACK, NULL,
			TG_TASK_PRIO, NULL);

	return result != pdPASS;
}

int tg_notify(const char *msg, enum bate_text_mode parse_mode)
{
	int status = -1;
	unsigned retries = TG_ERROR_MAX;

	if (tg.chat_id[0]) {
		do {
			status = bate_msg_send(tg.chat_id, msg, parse_mode,
					NULL);
			retries--;
		} while (200 != status && retries);
	} else {
		LOGD("no client, ignoring notification");
	}

	return status;
}

void tg_parser_stop(void)
{
	tg.parser_enable = false;
}
